# Events manager

## Purpose

This is a rework of an old technical test.

Point was to develop a small feature which stores and retrieves a
bunch of events from a database.

Event.py and eventController.py were written to this purpose around October
2019; I have yet to refactor them for real. For now all I did were a few
changes to make it work with the new features (and a proper database
yaay).

I plan on redoing this with a framework like django or flask, thus
adding a GUI.

In the early stage of refactoring it, I mostly wanted to practise with
docker, docker-compose and gitlab pipelines doing them from scratch.

## Pre-requisites

- docker version 20.10.6
- docker-compose version 1.29.1

## Get it

`git clone https://gitlab.com/tomkl/events-manager.git`

`cd events-manager/docker`

### Build from source

`docker-compose build`

### Pull the image

`docker-compose pull`

## Run it

This small app doesn't do much for now: Mostly, it executes
a script to store random events, and shows you events between a timelpse.

To run and see it, you have to launch these commands from events-manager/docker:

`docker-compose up -d`

`docker-compose exec events-app python3 /opt/app/bunch.py`
