import datetime
import random

from controllers.eventController import EventController

store = EventController()

start_ts = datetime.datetime(2000, 1, 1).timestamp()
end_ts = datetime.datetime(2020, 1, 1).timestamp()

for i in range(50):
    dt = datetime.datetime.fromtimestamp(
        random.randint(start_ts, end_ts)
    )
    store.store_event(at=dt, description='Event number %d.' % i)


for event in store.get_events(
    start=datetime.datetime(year=2000, month=1, day=1),
    end=datetime.datetime(year=2021, month=2, day=1)
):
    print(event)
