import pytz
from model.Event import Event


# Stores events & returns them according to a lapse of time

class EventController():
    def __init__(self):
        self.storage = {}
        self.list_events = []

    @property
    def storage(self):
        return self.__storage

    @storage.setter
    def storage(self, pobject):
        self.__storage = pobject

    @property
    def list_events(self):
        return self.__list_events

    @list_events.setter
    def list_events(self, string):
        self.__list_events = string

    # Adds events to the dictionary
    def store_event(self, at, description):
        # Creates a new Event object first
        obj = Event(at, description)

        # Then adds it to the dict
        i = len(self.storage)
        self.storage[i] = obj

    # Returns a list of events in a time lapse
    def get_events(self, start, end):
        self.list_events = []
        start = pytz.utc.localize(start)
        end = pytz.utc.localize(end)

        # Go through the dict
        for value in self.storage.values():
            if start <= value.datetime <= end:
                # Changes the values into strings
                to_string = str(value.datetime) + " : " + \
                    str(value.description)
                self.list_events.append(to_string)
        return self.list_events
