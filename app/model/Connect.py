import os
import psycopg2


class Connect():
    def __init__(self):
        self.conn = psycopg2.connect(
            host=os.getenv('DB_SERVER'),
            database="eventsmgr",
            user="eventsmgr",
            password="eventsmgr"
        )
        self.cur = self.conn.cursor()

    def query(self, query):
        self.cur.execute(query)

    def getVersion(self):
        self.query('SELECT version()')
        db_version = self.cur.fetchone()

        return db_version

    def close(self):
        self.cur.close()
        self.conn.close()
