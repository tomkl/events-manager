import pytz
from datetime import datetime
from model.Connect import Connect


# My events. They have a datetime and a string to describe them.

class Event():
    def __init__(self, pdatetime, pdescription):
        self.db = Connect()
        self.datetime = pdatetime
        self.description = pdescription

        self.setEvent()

    @property
    def id(self):
        return self.__id

    @property
    def datetime(self):
        return self.__datetime

    @datetime.setter
    def datetime(self, pdatetime):
        try:
            if type(pdatetime) is not datetime:
                raise TypeError
        except TypeError:
            print("Did not set a datetime")
            raise SystemExit()
        else:
            self.__datetime = pytz.utc.localize(pdatetime)

    @property
    def description(self):
        return self.__description

    @description.setter
    def description(self, pdescription):
        self.__description = pdescription

    def getAll(self, query):
        return db.query(query)

    # Update

    # Delete

    def setEvent(self):
        self.db.query(
            "INSERT INTO \
                event (datetime, description) \
                VALUES('{}', '{}')"
            .format(self.datetime, self.description)
        )
        self.db.conn.commit()
