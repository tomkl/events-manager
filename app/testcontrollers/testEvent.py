import unittest
import random
import datetime
from model.Event import Event


class EventTestCase(unittest.TestCase):
    @unittest.expectedFailure
    def test_fail_set_event(self):
        event1 = Event('aaaaaaa', 'bbbbbbbb')

    def test_set_event(self):
        start_ts = datetime.datetime(2000, 1, 1).timestamp()
        end_ts = datetime.datetime(2020, 1, 1).timestamp()
        dt = datetime.datetime.fromtimestamp(
            random.randint(start_ts, end_ts)
        )
        event1 = Event(dt, 'cccccccc')
        event1 = Event(dt, '')
        event1 = Event(dt, 1)

    @unittest.expectedFailure
    def test_fail(self):
        self.assertEqual(1, 0, "broken")


if __name__ == '__main__':
    unittest.main()
