import unittest
import random
import datetime
from controllers.eventController import EventController


class EventControllerTestCase(unittest.TestCase):
    store = EventController()
    start_ts = datetime.datetime(2000, 1, 1).timestamp()
    end_ts = datetime.datetime(2020, 1, 1).timestamp()

    def test_store_events(self):
        dt = datetime.datetime.fromtimestamp(
            random.randint(self.start_ts, self.end_ts)
        )
        self.store.store_event(at=dt, description='Event number %d.' % 1)

    def test_get_events(self):
        # Doesn't work
        for event in self.store.get_events(
            start=datetime.datetime(year=2000, month=1, day=1),
            end=datetime.datetime(year=2021, month=2, day=1)
        ):
            return event

    @unittest.expectedFailure
    def test_fail(self):
        self.assertEqual(1, 0, "broken")


if __name__ == '__main__':
    unittest.main()
