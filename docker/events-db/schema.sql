-- ----------------------------------------------------------------------------
-- Tables

CREATE TABLE IF NOT EXISTS event
(
    id          SERIAL,
    datetime    TIMESTAMPTZ NOT NULL,
    description VARCHAR(255),

    CONSTRAINT pk_event PRIMARY KEY(id)
);
